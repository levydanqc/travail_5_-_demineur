/* eslint-disable import/extensions */
/* eslint-disable no-unused-vars */
/* eslint-disable strict */

'use strict';

/* eslint-disable no-undef */
import JeuDemineur from './JeuDemineur.js';

$('#jeu').hide();
$('#instructions').hide();
$('#minuteur').hide();

let estSoumis = false;
let game;
let timer;
let checkTimer;
let secondes;
const utilisateur = {};
utilisateur.partieGagnees = 0;
let params;

/* Timer functions */

/**
 * @description Convertir un nombre de secondes en
 * une chaine de caractères correspodant à la minuterie.
 *
 * @return {string} La chaine de caractère de temps restant.
 */
function ObtenirChaineMinuterie() {
  let minutes = Math.floor(secondes / 60);
  if (minutes < 10) {
    minutes = `0${minutes}`;
  }
  let secondesRestantes = secondes - minutes * 60;
  if (secondesRestantes < 10) {
    secondesRestantes = `0${secondesRestantes}`;
  }
  return `${minutes}:${secondesRestantes}`;
}

/**
 * @description Afficher le minuteur dans la page.
 *
 */
function AfficherMinuteur() {
  $('#minuteur').html(ObtenirChaineMinuterie())
    .show();
}

/**
 * @description Mettre-à-jour le minuteur en enlevant une seconde.
 *
 */
function MajMinuterie() {
  if (secondes > 0) secondes -= 1;
  AfficherMinuteur();
}

/**
 * @description Démarrer le minuteur pour une mise-à-jour à chaque
 * seconde.
 *
 */
function DemarrerMinuterie() {
  timer = setInterval(MajMinuterie, 1000);
}
/* END Timer */

/**
 * @description Obtenir les paramètres du jeu définis par l'utilisateur.
 *
 * @return {Object} Nombre de rangées, nombre de colonnes, nombre de mines.
 */
function ObtenirParametres() {
  const nbRangees = parseInt($('#nbRangees').val(), 10);
  const nbColonnes = parseInt($('#nbColonnes').val(), 10);
  const nbMines = parseInt($('#nbMines').val(), 10);

  return { nbRangees, nbColonnes, nbMines };
}

/**
 * @description Revenir à l'écran d'acceuil du choix des paramètres de jeu.
 *
 */
function Revenir() {
  $('#jeu').empty();
  $('#instructions').hide();
  $('#result').empty();
  $('#minuteur').hide();
  utilisateur.nbClick = 0;
  $('#jeu').hide();
  $('#back').css('transform', 'translateX(-100vw)');
  $('#submit').html('Valider');
  $('#params').show();
}

/**
 * @description Affiche un message lorsque le jeu est terminé.
 *
 * @param {string} message Message de résultat du jeu.
 */
function MessageFin(message) {
  if ($('h2').length === 0) { // Message n'a pas été affiché
    // Arrêter le minuteur
    clearInterval(timer);
    clearInterval(checkTimer);

    $('#jeu input').attr('disabled', 'disabled');
    $('#back').css('transform', 'none');
    $('#back').on('click', Revenir);

    if (message === 'Gagné') utilisateur.partieGagnees += 1;

    const listeResult = $('<ul></ul>');
    const nbClick = $(`<li>Nombre de cases cliquées : ${utilisateur.nbClick}</li>`);
    const nbMines = $(`<li>Nombre de mines : ${utilisateur.nbMines}</li>`);
    const totalCases = $(`<li>Nombre de cases totales : ${utilisateur.totalCases}</li>`);
    const partieGagnees = $(`<li>Nombre de parties gagnées : ${utilisateur.partieGagnees}</li>`);
    listeResult.append(nbClick, nbMines, totalCases, partieGagnees);
    const result = $(`<h2>${message} !</h2>`);
    const classResultat = (message === 'Perdu') ? 'text-danger' : 'text-success';
    result.addClass(classResultat);
    $('#result').append(result);
    $('#result').append(listeResult);

    $('#jeu input').attr('disabled', 'disabled');
    $('#jeu input').removeClass('btnClick');

    $('#submit').html('Recommencer');
  }
}

/**
 * @description Vérification de l'état du minuteur.
 *
 */
function CheckMinuteur() {
  if ($('#minuteur').html() === '00:00') MessageFin('Perdu');
}

/**
 * @description Fonction récursive afin de révéler les cases vides alentours
 * lors d'un click sur une case vide.
 *
 * @param {int} x Position en x de la case.
 * @param {int} y Position en y de la case.
 */
function CheckCasesAutour(x, y) {
  // const bordGauche = (x === 0);
  // const bordDroit = (x === params.nbColonnes - 1);
  // const bordHaut = (y === 0);
  // const bordBas = (y === params.nbRangees - 1);

  for (let xOff = -1; xOff <= 1; xOff += 1) {
    for (let yOff = -1; yOff <= 1; yOff += 1) {
      const i = x + xOff;
      const j = y + yOff;
      if ((i > -1 && i < params.nbRangees)
        && (j > -1 && j < params.nbColonnes)
        && game.ContenuCase(j, i) !== game.carMine
        && $(`input[data-x=${i}][data-y=${j}]`).attr('disabled') !== 'disabled') {
        // eslint-disable-next-line no-use-before-define
        ButtonClick(null, i, j);
      }
    }
  }
}

/**
 * @description Gestion d'un click gauche sur une des cases du jeu.
 *
 * @param {Event} [e] Évênement du click de l'utilisateur.
 * @param {string || int} [xCase=null] Position en x de la case.
 * @param {string || int} [yCase=null] Position en y de la case.
 */
function ButtonClick(e, xCase = null, yCase = null) {
  // Démarrer le minuteur s'il n'est pas en cours
  if (!utilisateur.minuteurEnCours) {
    DemarrerMinuterie();
    checkTimer = setInterval(CheckMinuteur, 1000);
    utilisateur.minuteurEnCours = true;
  }
  utilisateur.nbCasesDecouvertes += 1;
  if (e) utilisateur.nbClick += 1;
  const x = (xCase === null) ? parseInt($(e.target).attr('data-x'), 10) : xCase;
  const y = (yCase === null) ? parseInt($(e.target).attr('data-y'), 10) : yCase;
  const element = $(`input[data-x=${x}][data-y=${y}]`);
  element.attr('disabled', 'disabled')
    .css('pointer-events', 'none')
    .css('box-shadow', 'none');
  const contenu = game.ContenuCase(y, x);

  let contenuCase = '';
  switch (contenu) {
    case 1:
      contenuCase = 'text-success';
      break;
    case 2:
      contenuCase = 'text-warning';
      break;
    case 3:
      contenuCase = 'text-danger';
      break;
    default: // undefined or mine
      contenuCase = '';
  }
  if ([1, 2, 3].includes(contenu)) {
    element.attr('value', `${contenu}`)
      .addClass(contenuCase);
  } else if (contenu === game.carMine) {
    element.attr('type', 'image')
      .attr('src', './assets/mine.png')
      .addClass('bg-danger');

    MessageFin('Perdu');
  } else { // Fonction recurcive
    CheckCasesAutour(parseInt(x, 10), parseInt(y, 10));
  }
  // Vérification de la fin de la partie
  const estFini = utilisateur.totalCases - utilisateur.nbMines === utilisateur.nbCasesDecouvertes;
  if (estFini) { // Dernière case
    MessageFin('Gagné');
  }
}

/**
 * @description Gestion d'un click droit sur une des cases du jeu.
 *
 * @param {Event} e Évênement du click de l'utilisateur.
 */
function ButtonRightClick(e) {
  // Démarrer le minuteur s'il n'est pas en cours
  if (!utilisateur.minuteurEnCours) {
    DemarrerMinuterie();
    checkTimer = setInterval(CheckMinuteur, 1000);
    utilisateur.minuteurEnCours = true;
  }
  if ($(e.target).attr('type') === 'button') {
    $(e.target).attr('type', 'image')
      .attr('src', './assets/flag.png')
      .off('click', ButtonClick)
      .removeClass('btnClick');
    if (game.ContenuCase($(e.target).attr('data-y'), $(e.target).attr('data-x')) === game.carMine
      && utilisateur.totalCases - utilisateur.nbCasesDecouvertes === 1) {
      MessageFin('Gagné');
    }
  } else {
    $(e.target).attr('type', 'button')
      .removeAttr('src')
      .on('click', ButtonClick)
      .addClass('btnClick');
  }
  e.preventDefault();
}

/**
 * @description Création et affichage d'un jeu démineur en fonction des
 * paramètres de l'utilisateur.
 *
 */
function CreationJeu() {
  if ($('#jeu').children().length === 0) {
    params = ObtenirParametres();
    utilisateur.totalCases = params.nbRangees * params.nbColonnes;
    utilisateur.nbMines = params.nbMines;
    utilisateur.nbClick = 0;
    utilisateur.nbCasesDecouvertes = 0;

    for (let i = 0; i < params.nbRangees; i += 1) {
      const row = $('<div></div>');
      row.addClass('row');
      for (let j = 0; j < params.nbColonnes; j += 1) {
        const button = $('<input type="button"/>');
        button.addClass('case');
        button.on('click', ButtonClick);
        button.on('contextmenu', ButtonRightClick);
        button.attr('data-x', `${i}`);
        button.attr('data-y', `${j}`);
        row.append(button);
      }
      $('#jeu').append(row);
    }

    // Création d'un jeu
    game = new JeuDemineur(params.nbMines, params.nbRangees, params.nbColonnes);
    game.GenererCase();

    // Affichage de la minuterie
    secondes = utilisateur.totalCases * 10;
    utilisateur.minuteurEnCours = false;
    AfficherMinuteur();

    $('#jeu').show();
    $('#instructions').show();
  }
}

/**
 * @description Gestion du bouton 'Recommencer' afin de recommencer une
 * partie avec les mêmes paramètres.
 *
 */
function Recommencer() {
  $('#jeu').empty();
  $('#result').empty();
  utilisateur.nbClick = 0;
  $('#back').css('transform', 'translateX(-100vw)');
  CreationJeu();
}

/**
 * @description Validation du formulaire des paramètres de jeu et
 * affichage des messages d'erreurs.
 *
 */
function AjouterValidations() {
  $('#form').validate(
    {
      rules: {
        nbRangees: {
          required: true,
          range: [2, 6],
        },
        nbColonnes: {
          required: true,
          range: [2, 6],
        },
        nbMines: {
          required: true,
          range: [1, 3],
        },
      },
      messages: {
        nbRangees: {
          required: 'Veuillez entrer le nombre de rangées désirées.',
          range: jQuery.validator.format('Veuillez entrer un nombre de rangées comprit entre {0} et {1}.'),
        },
        nbColonnes: {
          required: 'Veuillez entrer le nombre de colonnes désirées.',
          range: jQuery.validator.format('Veuillez entrer un nombre de colonnes comprit entre {0} et {1}.'),
        },
        nbMines: {
          required: 'Veuillez entrer le nombre de mines désirées.',
          range: jQuery.validator.format('Veuillez entrer un nombre de mines comprit entre {0} et {1}.'),
        },
      },
      submitHandler(form) {
        $('#params').hide();
        $('#liste-erreurs').empty();
        if ($('#submit').html() === 'Valider') {
          CreationJeu();
        } else {
          Recommencer();
        }
      },
      showErrors(errorMap, errorList) {
        if (estSoumis) {
          // Nettoyage des anciens messages d'erreurs
          const liste = $('#liste-erreurs');
          if (liste.children().length > 0) {
            liste.empty();
          }

          const titre = $('<p>Veuillez corriger les erreurs suivantes :</p>');
          titre.addClass('h3');
          liste.append(titre);
          const ul = $('<ul></ul>');
          ul.addClass('liste-erreurs');
          $.each(errorList, function AjoutErreurs() { ul.append(`<li>${this.message}</li>`); });
          liste.append(ul);
          estSoumis = false;
        }
        this.defaultShowErrors();
      },
      invalidHandler(form, validator) {
        estSoumis = true;
      },
    },
  );
}

/**
 * Initialisation de la fonction de validation après le chargement
 * de la page.
*/
$(() => {
  AjouterValidations();
});
