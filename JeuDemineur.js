/* eslint-disable max-classes-per-file */

class JeuDemineur {
  constructor(nbMines, nbRangees, nbColonnes) {
    this.nbColonnes = nbColonnes;
    this.nbMines = nbMines;
    this.nbRangees = nbRangees;
    this.carMine = '*';
    this.carMineExplose = '!';
    this.carCaseNettoyee = 'X';
    this.listeMines = [];
    this.listeCoups = [];
  }

  GenererCase() {
    this.tableau = new Array(this.nbRangees);
    for (let i = 0; i < this.nbRangees; i++) {
      this.tableau[i] = new Array(this.nbColonnes);
    }
    this.GenererCasesMines();
    this.GenererNombres();
  }

  GenererNombres() {
    for (let i = 0; i < this.nbRangees; i++) {
      for (let j = 0; j < this.nbColonnes; j++) {
        if (this.tableau[i][j] === this.carMine) {
          this.AjouterUnAutour(i, j);
        }
      }
    }
  }

  AjouterUnAutour(x, y) {
    const indexHorizontalMin = (y === 0) ? 0 : y - 1;
    const indexVerticalMin = (x === 0) ? 0 : x - 1;
    const indexHorizontalMax = (y === this.nbColonnes - 1) ? this.nbColonnes - 1 : y + 1;
    const indexVerticalMax = (x === this.nbRangees - 1) ? this.nbRangees - 1 : x + 1;
    for (let i = indexVerticalMin; i <= indexVerticalMax; i++) {
      for (let j = indexHorizontalMin; j <= indexHorizontalMax; j++) {
        const courant = this.tableau[i][j];
        if (courant !== this.carMine) {
          if (!isNaN(courant)) {
            this.tableau[i][j] = courant + 1;
          } else {
            this.tableau[i][j] = 1;
          }
        }
      }
    }
  }

  ContenuCase(x, y) {
    return this.tableau[y][x];
  }

  GenererCasesMines() {
    let nbMinesPlacees = 0;
    while (nbMinesPlacees < this.nbMines) {
      const xAleatoire = Math.floor(Math.random() * this.nbColonnes);
      const yAleatoire = Math.floor(Math.random() * this.nbRangees);
      const contenuCase = this.tableau[xAleatoire][yAleatoire];
      if (contenuCase !== this.carMine) {
        this.tableau[xAleatoire][yAleatoire] = this.carMine;
        nbMinesPlacees++;
        this.listeMines.push(new Case(xAleatoire, yAleatoire));
      }
    }
  }
}

class Case {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
}

export default JeuDemineur;
